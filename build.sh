#!/bin/bash

shopt -s expand_aliases

LOG_FILE="build.log"
TMP_REJ_FILE="rejects"

echo "Build started at " $(date) > $LOG_FILE

handle_git_clone_error() {
	if [ $1 != 0 ]; then
		echo ""
		if [ $1 == 128 ]; then
			echo "FATAL: Git repository not found. Please make sure you're logged in with your git client"
			echo "and that you got the proper permissions to access the repos."
		else
			echo "FATAL: Git error. Please contact the team on #engineering"
		fi
		exit 1
	fi
}

handle_git_checkout_error() {
	if [ $1 != 0 ]; then
		echo ""
		echo "FATAL: Git error. Probably you have uncommited changes."
		echo "Please clean your local repo state and try again."
		exit 1
	fi
}

gitcheckout() {
	cd $1
	br=`git branch | grep "*"`
	if [[ ${br/* /} != $2 ]]; then
		echo "Checkout to the ${2} branch..."
		git checkout $2
		handle_git_checkout_error $?
	fi
	cd ..
}

# Shamelessly stolen from
# https://stackoverflow.com/questions/4023830/how-to-compare-two-strings-in-dot-separated-version-format-in-bash
vercomp() {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

patch_node_modules() {
	# Remove iframes
	patch -N -r $TMP_REJ_FILE mr-metarouter/node_modules/@astronomerio/analytics.js-integration/lib/protos.js < thd-patches/protos-js.patch
	patch -N -r $TMP_REJ_FILE mr-metarouter/node_modules/@astronomerio/analytics.js-integration/package.json < thd-patches/analytics-js-integration-package-json.patch

	for dir in "analytics-js-core" "mr-metarouter"; do
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/@segment/store/src/store.js < thd-patches/segment-store-store-js.patch
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/segmentio-facade/lib/facade.js < thd-patches/facade.patch
	done

	for dir in "analytics-js" "analytics-js-builder" "analytics-js-core" "mr-metarouter"; do
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/path-browserify/index.js < thd-patches/path-browserify.patch
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/buffer/index.js < thd-patches/buffer-index-js.patch
		patch -N -r $TMP_REJ_FILE $dir/node_modules/trim/index.js < thd-patches/trim.patch
	done

	for dir in "analytics-js-core" "mr-metarouter"; do
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/json3/lib/json3.js < thd-patches/json3.patch
	done
	
	for dir in "analytics-js" "analytics-js-builder"; do
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/json3/lib/json3.js < thd-patches/analytics-js-json3.patch
	done

	for dir in "analytics-js-core" "analytics-sync-injector"; do
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/crypto-js/core.js < thd-patches/crypto-js-core.patch
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/crypto-js/md5.js < thd-patches/crypto-js-md5.patch
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/crypto-js/sha256.js < thd-patches/crypto-js-sha256.patch
	  patch -N -r $TMP_REJ_FILE $dir/node_modules/crypto-js/index.js < thd-patches/crypto-js-index.patch
	done
}

clear

needclone=0
for dir in "mr-metarouter" "analytics-sync-injector" "analytics-js-core" "analytics-js" "analytics-js-builder"; do
	if [[ ! -d $dir ]]; then
		needclone=1
	fi
done

if [[ $needclone == 1 ]]; then
	echo "* Welcome the THD Analytics.js file builder! *"
	echo ""
	echo "Please note: it is recommended to run this script into a clean environment."
	echo "Report any bugs to the engineering Slack channel."
	echo ""
	read -n 1 -s -r -p $'Press any key to continue, or press Escape to cancel\n' KEY

	case $KEY in
	     $'\e') echo; exit 1;;
	esac
	clear
fi

echo "Checking for deps..."
if [[ "$OSTYPE" == "darwin"* ]]; then
	if ! [ -x "$(command -v gsed)" ]; then
	  echo 'Error: gnu sed is not installed. Install it with `brew install gnu-sed`' >&2
	  exit 1
	fi

	alias sed=gsed
fi

nodev=$(node -v)
vercomp "${nodev:1}" "8.17.0"
if [ $? == 1 ]; then
	echo "Your node version is ${nodev}. Please use a version at most v8.17.0, as" >&2
	echo "dtrace is known to fail building on version greater than this" >&2
	echo
	echo "PRO TIP: Use nvm to switch between Node.js versions"
	exit 1
fi

if [[ ! -d "compiler-latest" ]]; then
	echo "Downloading closure compiler..."
	curl "https://dl.google.com/closure-compiler/compiler-20200719.zip" -o compiler-latest.zip
	echo "Unzipping..."
	unzip -q compiler-latest.zip -d compiler-latest
	rm compiler-latest.zip
	mv compiler-latest/closure-compiler*.jar compiler-latest/closure-compiler.jar
	echo "Done."
fi

if [[ $needclone == 1 ]]; then
	echo "Cloning the required repos..."
	if [[ !  -d "analytics-js-builder" ]]; then
		git clone https://gitlab.com/metarouter/analytics-js-builder.git >> $LOG_FILE 2>&1
		handle_git_clone_error "$?"
		gitcheckout analytics-js-builder homedepot-builder
	fi
	if [[ ! -d "analytics-js" ]]; then
		git clone https://gitlab.com/metarouter/analytics-js.git >> $LOG_FILE 2>&1
		handle_git_clone_error "$?"
		gitcheckout analytics-js homedepot-build
	fi
	if [[ ! -d "analytics-js-core" ]]; then
		git clone https://gitlab.com/metarouter/analytics-js-core.git >> $LOG_FILE 2>&1
		handle_git_clone_error "$?"
		gitcheckout analytics-js-core homedepot-core
	fi
	if [[ ! -d "analytics-sync-injector" ]]; then
		git clone https://gitlab.com/metarouter/analytics-sync-injector.git >> $LOG_FILE 2>&1
		handle_git_clone_error "$?"
		gitcheckout analytics-sync-injector complience-module
	fi
	if [[ ! -d "mr-metarouter" ]]; then
		git clone https://gitlab.com/metarouter/client-logic/mr-metarouter.git >> $LOG_FILE 2>&1
		handle_git_clone_error "$?"
	fi
fi

if [[ ! -f "analytics-js-builder/.env" ]]; then
	clear
	echo "You need to specify the required environment variables for the Analytics.js builder."
	echo "Environment variables for this file can be requested from another member of the integration team."
	echo
	echo "An editor will open with the file loaded, please fill it with the required env variables."

	read -n 1 -s -r -p $'Press any key to continue, or press Escape to cancel\n' KEY
	case $KEY in
	     $'\e') echo; exit 1;;
	esac

	cd analytics-js-builder
	touch .env
	"${EDITOR:-vi}" .env
	cd ..
	clear
fi

patch -N -r $TMP_REJ_FILE analytics-js/package.json < thd-patches/analytics-js-package-json.patch
patch -N -r $TMP_REJ_FILE analytics-js-builder/package.json < thd-patches/analytics-js-builder-package-json.patch
patch -N -r $TMP_REJ_FILE analytics-js-core/package.json < thd-patches/analytics-js-core-package-json.patch

needs_npm_i=0
for dir in "mr-metarouter" "analytics-sync-injector" "analytics-js-core" "analytics-js" "analytics-js-builder"; do
	if [[ ! -d "${dir}/node_modules" ]]; then
		needs_npm_i=1
		echo "Installing Node packages for (this may take a while, please watch output for errors)..."
		cd $dir
		npm install --silent
		cd ..
	fi
done

if [[ $needs_npm_i == 1 ]]; then
	patch_node_modules
	# if [[ ! -f cleanup_node_modules.sh ]]; then
	# 	echo "Downloading cleanup script..."
	# 	curl "https://gitlab.com/ov1d1u/thd-scripts/-/raw/master/cleanup_node_modules.sh" -o cleanup_node_modules.sh
	# 	chmod +x cleanup_node_modules.sh
	# fi

	# echo "Cleaning up the node modules"
	# 	./cleanup_node_modules.sh
fi

# ACTUAL BUILDER

echo "Building the Analytics.JS file for THD..."

cd analytics-sync-injector
npm run build
cd ..

cd analytics-js-builder
node bin/boot.dev
cd ..

cd compiler-latest
echo "Closure compiler result:" >> $LOG_FILE 2>&1
curl --compressed https://meta-cdn.s3.amazonaws.com/analytics.js/v1/THD1/THD1.js --output THD1.js >> $LOG_FILE 2>&1
patch -N -r $TMP_REJ_FILE THD1.js < ../thd-patches/thd1.patch
java -jar closure-compiler.jar --js THD1.js --js_output_file ./output/THD1.js
buildprod=
while :; do
	case $1 in
		--prod)
			buildprod=1
			;;
		*)
			break
	esac

	shift
done

if [[ $buildprod -ne 1 ]]; then
	sed -i '1s/^/var init = function () {\n\/\* eslint-disable \*\/\n/' ./output/THD1.js
	echo "/* eslint-enable */
	};
	export {
		init
	}" >> ./output/THD1.js
fi

cd ..
if [[ -f $TMP_REJ_FILE ]]; then
	rm $TMP_REJ_FILE
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
  # Open Finder on MacOS
  open ./compiler-latest/output/
fi

if [[ $buildprod -ne 1 ]]; then
	echo ""
	echo "!!WARNING!!"
	echo "The resulted file is not suitable to be sent to THD!"
	echo "Call this script with --prod flag to build production builds."
	echo ""
fi